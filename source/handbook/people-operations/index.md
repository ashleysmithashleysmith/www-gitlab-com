---
layout: markdown_page
title: "People Operations"
---

- [Reaching People Operations](#reach-peopleops)
- [Setting up new contracts](#new-contracts)
   - [Using HelloSign](#hellosign)
- [Using BambooHR](#bamboohr)
   - [Adding a new team member to BambooHR](#admin-bamboo)
   - [Using BambooHR as a new team member](#new-member-bamboo)
- [Administrative details of benefits for US-based employees](#benefits-us)
   - [401k](#401k)
- [Using TriNet](#using-trinet)
   - [Add new hires to TriNet](#trinet-process)
   - [Making changes in TriNet](#changes-trinet)

## Reaching People Operations<a name="reach-peopleops"></a>

To reach People Operations, please post an issue on our [internal issue tracker](https://dev.gitlab.org/gitlab/organization/issues/)
and add the 'PeopleOps' label, or send an email to the People Operations group (see the "GitLab Email Forwarding" google doc), or ping an individual
member of the People Operations team, as listed on our [Team page](https://about.gitlab.com/team/).

## Setting up new hiring contracts<a name="new-contracts"></a>

New team hire contracts are found on the [Contracts](https://about.gitlab.com/handbook/contracts/) page, including instructions on how to set up new contracts.

### Using HelloSign<a name="hellosign"></a>

When we need [contracts to be signed](https://about.gitlab.com/handbook/#signing-legal-documents) we use [HelloSign](https://hellosign.com).
Follow these steps to send out a signature request.

1. Choose who needs to sign. (just me, me & others, or just others)
1. Upload your document that needs signing
1. Enter the names of the people that need to sign
1. With more than one signature required, assign signing order (for contracts or offer letters, always have the GitLab Signatory sign first)
1. Add the People Ops team to the cc
1. Click on "Prepare Docs for signing"
1. Drag & Drop the Signature and Date fields to the corresponding empty spaces in the document (you can select the signee with the pop up screen)
1. Add a title and a message for the recipient. For contract use for example: "Dear [...], You can sign [document type] with HelloSign. Once you've signed you will receive a copy by email. If you have any questions, feel free to reach out"
1. Request Signature.

Once you've sent out the document you will receive email notifications of the progress and a copy of the signed document after all parties have signed.

## Using BambooHR<a name="bamboohr"></a>

We’re using [BambooHR](https://gitlab.bamboohr.com) to keep all team member information 
in one place. All team members (all contract types) are in BambooHR.

Some changes or additions we make to BambooHR require action from our team members.
Before calling the whole team to action:  

- make sure _that it is necessary_  for the entire team to act, or whether the
work can be done by People Ops (even if this is tedious to do for People Ops, it
is preferred, so as not to burden the team),
- make sure that the call to action is OK-ed by management, 
- and check the steps to be taken by testing them with a single team member or a
test account that does not have admin privileges.

### Adding a new team member to BambooHR <a name="admin-bamboo"></a>

After People Ops is requested to make a contract or offer letter for a potential hire,
as described on our [Hiring](/handbook/hiring/) page, follow these steps:

1. Download the correct template from the files page on BambooHR
1. Edit the contract or offer letter to include the new hire information.
1. Send out the document to be signed using HelloSign, \cc people ops in this process. (document HelloSign first)
1. Once the document has been signed, create a BambooHR employee file:
   1. Go to the BambooHR Dashboard
   1. Click on the top right on “Add employee”
   1. Enter the following fields: First name, Last name, Gender, Hire date, Employment status, Job information, Pay rate, Salary for employees, or select another option.
   1. Turn “self service” on for the employee to enter the following info: Address, Phone, DOB. private email.
1. Save the signed contract under the Documents tab in the folder “0.Signed Contract/Offer Letter”.

### Using BambooHR as a new team member<a name="new-member-bamboo"></a>

Team members can keep their information in BambooHR updated using self-service:

1. Go to the BambooHR link that you should have received by email
1. Change the password to make it secure (see the handbook section on [secure passwords](https://about.gitlab.com/handbook/security/))
1. Login with your new credentials
1. Keep your credentials stored in your 1Password Vault and keep the info in BambooHR updated (e.g. change address when moving)

## Administrative details of benefits for US-based employees <a name="benefits-us"></a>

### 401k<a name="401k"></a>

1. You are eligible to participate in GitLab’s 401k as of the 1st of the month after your hire date.  
1. You will receive a notification on your homepage in Trinet Passport once eligible,
if you follow the prompts it will take you to the Transamerica website https://www.ta-retirement.com/
or skip logging in to Trinet Passport and go directly to https://www.ta-retirement.com/
after the 1st of the month after your hire date.
1. Once on the home page of https://www.ta-retirement.com/ go to "First Time User Register Here".  
1. You will be prompted for the following information
   1. Full Name
   1. Social Security Number
   1. Date of Birth
   1. Zip Code  
1. Once inside the portal you may elect your annual/pay-period contributions, and Investments.

## Using TriNet<a name="using-trinet"></a>

### Entering New Hires into TriNet<a name="trinet-process"></a>

Employer enters the employee data in the HR Passport with the information below

1. Under the My Staff tab- select new hire/rehire and a drop down menu will appear.
1. Enter all of the necessary information:
    * Company name autopopulates
    * SS
    * Form of address for hire (Mr. Ms, etc.)
    * First name
    * Last name
    * Middle name or initial
    * Country
    * Address
    * Home phone
    * Home email
    * Gener
    * Ethnicity (you must select something - guess if employee declines to state)
    * Military status

At the bottom of the screen, select next

    * TriNet’s start date
    * Reason - drop down menu with options
    * Employment type - Full time or PT options
    * Select reg/temp bubble
    * Employee Class - drop down between regular and commission
    * Estimated annual wages (does not include anything besides base salary)
    * Benefit class
    * Future benefits class -
    * Standard Hours/week - Part time or Full time
    * Business Title - see org chart
    * Job Code - no need to enter anything here
    * FLSA status- drop down options are exempt, non-exempt, computer prof-non-exempt, computer prof- exempt
    * Supervisor - drop down menu of names
    * Compensation Basis
    * Compensation Rate
    * Departments
    * Work Location - drop down menu
    * Pay Group - only one option
    * Employee ID - not necessary
    * Work email
    * Grouping A/level - not necessary
    * Grouping B/sponsor- not necessary

Select next or save (if you select save, it will hold your information)

    * Vacation/PTO - drop down menu only provides one option- select this
    * Sick- drop down menu only provides one option- select this
    * Personal Time - leave blank
    * Floating Holidays - leave blank
    * Birthdate - mm/dd/yyyy
    * Workers compensation- select unknown and it will default to our principle class code for our industry
Window: Describe employees job duties - simple description

After submission -  you will receive a prompt for final submission, select and submit.

Note: if you save the information to finish at a later date, go to the Work Inbox and select New Hires Not Submitted to continue.

1. The employee receives a welcome email the night before their start date.
1. The employee is prompted to log on, complete tax withholding (W4 data), direct deposit information, section 1 of the I-9, and benefits election (if eligible).
1. The employer logs in to HR Passport and is prompted by way of work inbox item, to complete section 2 of the I-9.

### Making changes in TriNet <a name="changes-trinet"></a>

#### Add a New Location

1. Go to HR Passport homepage
1. Click Find
1. Click Find Location.
1. When search field appears, leave blank and click Search.
1. Click on Add location.
1. Complete location information. For a remote location, enter the location (ex. WA remote) in all fields except city, state and zip.
1. Click Add.

#### Transfer Employee to Different Location

1. Go to HR Passport homepage.
1. Click Find.
1. Select find person by Name.
1. Type the name, click search.
1. From the choices, select the name.
1. On the left side of the screen, select Employment Data.
1. Select Employee Transfer.
1. Change location and fill in necessary information.
1. Select Update.
