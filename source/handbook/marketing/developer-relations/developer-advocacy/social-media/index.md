---
layout: markdown_page
title: "Developer Advocacy - Social Media"
---

## Social Media Channels

GitLab has a presence across many social media channels. The following is a list of channels we actively monitor.

* [The GitLab blog](#)
* [GitLab issue trackers](#)
* [Twitter](#)
* [Facebook](#)
* [LinkedIn](#)
* [Hacker News](#)
* [Quora](#)
* [Reddit](#)
* [IRC](#)
* [Stack Overflow](#)
* [Slack](#)

## Responding to Requests

Any time GitLab is mentioned or relevant to a thread, someone from GitLab should be responding to questions or feedback. The idea here is to be available for people without making them feel obligated to talk to you. Remain positive.

- **Remember that you’re speaking to a human being.** The people behind the comments are real, so treat them how you’d want to be treated if the roles were reversed. Try to find their real name so that you can personalize your message to them.
- **Begin by thanking them for their feedback or apologizing to them for any inconvenience they may have experienced.** We want to start and end conversations on a high note.
- **Provide meaningful responses instead of just upvoting or saying “Thanks!”** If a post will add to a discussion, post it. If someone has given you feedback, link them to a relevant issue showing the feedback has been received. You may even provide people with links to non-GitLab things such as Git guides or DevOps guides.
- **Assume good faith**. Remember, people are commenting on the product, not the people. The line can get blurry, but it’s important to stay objective.
- **Address any and all points the user has made.** If they made 4 points/requests, respond to each. If you don’t know the answer, don’t be afraid to tell them you don’t know but will look into it.
- **You’re allowed to disagree with people.** Try to inform the person (respectfully) why they might be misguided and provide any links they may need to make a more informed decision. Don’t say “you’re wrong” or “that’s stupid.” Instead try to say “I disagree because…” or “I don’t think that’s accurate because…”
- **Replies are not endorsements.** Just because you’re replying and not publicly disagreeing doesn’t mean you agree with the statement.
- **Always seek feedback.** If someone has something negative to say, ask them how we could make it better. If they can provide examples that’s even better.
- **Open issues!** If someone has a point they’d like to discuss, feel free to open an issue and link them to it. Regardless of whether or not you agree with the point, you should be inviting the community to participate in GitLab’s direction. Be sure to link to the relevant post in the issue for easier tracking. This won’t work for all cases, so use your best judgement.
- **Do what you say you will.** If you say you'll look into something, set a reminder to circle back with the person or link them to an issue on the issue tracker. Saying you'll look into something makes it impossible to continue the conversation until you do so, so it is essential that you continue the conversation at some later point. If this is too much work just say 'I don't know' and leave it at that.

## GitLab Voice

You are personally responsible for the tweets, likes and replies you post on social media while representing GitLab. Everything you publish is publicly viewable and will be available for a long time even if redacted. Be careful and thoughtful when using the company accounts.

When speaking for GitLab, use the “GitLab voice.” When replying from the official GitLab account, speak as “we” and represent the software and community. On the official @GitLab account Twitter account and in other social media we should model attributes of our software and community. We strive to respond to all messages and questions. We respond by encouraging collaboration and contribution.

Consider what benefits the software and community, and how the software would respond if we personified it. Be responsive, positive, open minded, curious, welcoming, apologetic, transparent, direct, and honest. Someone doesn’t like something? Ask them to tell us more in the issue tracker. Someone thinks GitLab could be better? Invite them to submit a feature proposal. Any criticism is an opportunity to improve our software.

When responding to posts from your personal account, feel free to incorporate your own style and voice. Talk to people as if you were talking to them in person.

## Dealing with Conflict

You may come across angry users from time to time. When dealing with people who are confrontational, it’s important to remain level-headed. You may also send them to Sid directly.

- Assume good faith. People have opinions and sometimes they’re strong ones. It’s usually not personal.
- If it’s getting personal, step away from the conversation and delegate to someone else.
- Sometimes all people need is acknowledgement. Saying “Sorry things aren’t working for you” can go a long way.


This document is a guide for GitLab team members who manage social media accounts. 

You are personally responsible for the tweets, likes and replies you post on social media while representing GitLab. Everything you publish is publicly viewable and will be available for a long time even if redacted. Be careful and thoughtful when using the company accounts. 

## Twitter

There are two GitLab Twitter accounts

-   [@GitLab](https://twitter.com/gitlab) -  This is the main account for the project and company, which is managed by team members at GitLab, Inc. Everything tweeted, RTd, or liked from @GitLab also gets promoted to http://about.gitlab.com

-   [@GitLabStatus](https://twitter.com/gitlabstatus) - This account tweets about the status of GitLab.com services and is managed by the GitLab operations team.
We don't generally retweet GitLabStatus, but point users to follow that account or check it.

### Representing GitLab on Twitter

Express gratitude

-   Favorite tweets which include positive comments about GitLab or articles mentioning GitLab in a positive way.

-   Thank users for feedback and comments with @mentions like: ‘Thank you’, ‘Glad to hear that’, ‘You’re welcome’, do this even when you already favored something.

Be friendly

-   When responding to tweets, be polite and brief.

-   It’s OK to invite CE or GitLab.com users to collaborate, the easiest way is to direct a user to the relevant issue tracker.

About using the tools

-   Zendesk is a good place to reply to tweets, but always check for the tweet history on Tweetdeck.

-   Use Tweetdeck to find GitLab mentions without the # or @.

Use English

- [GitLab communicates in English](https://about.gitlab.com/handbook/#internal-communication) so please tweet in English.

- If a foreign language tweet comes in, replies in that language are okay but discouraged as we can't guarantee making our support SLA.

- Please no foreign retweets, these break the flow of people reading their timeline and cause people to unfollow.

### Replies to commonly asked questions on Twitter

-   Feature proposals: It would be really cool if GitLab could… 

    -   If this is a first time feature request: Ask them to submit a feature request on the issue tracker for [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/issues) or [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee/issues) and add the label <code>feature proposal</code>

    -   If there’s an existing feature request, ask the user to vote on the request with a link to the feature request.

    -   If we’re accepting merge requests, tell them with a link to the feature request. Often the feature request thread will indicate that as well.

-   Bug reports: I think I’ve discovered a bug. 

    -   Bug reports are welcome. Direct them to this guide on reporting a bug. <https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md>

-   Product specific questions: I need some help with X.

    -   Check if there is existing documentation and reply with a link if there is.

    -   GitLab.com specific: Ask the user to add the Issue to our GitLab.com Support Forum. https://gitlab.com/gitlab-com/support-forum/issues

    -   GitLab CE or CI: ask the user to add the Issue to our CE Issue tracker. https://gitlab.com/gitlab-org/gitlab-ce/issues

-   Support or help: I think I ran into a problem.

    -   Respond with a quote and CC [@GitLabSupport](http://twitter.com/gitlabsupport). Using a quote means pasting the full URL to the author’s original tweet into your reply. Check to make sure you are also replying directly to their account and not yourself.

-   GitLab is down!

    -   If there is a known issue, apologize and invite them to follow [@GitLabStatus](https://twitter.com/gitlabstatus)

    -   If this is not known, alert the operations team, and thank the reporter. 

-   Request for consulting or development.

    - If a GitLab user would like to engage the GitLab team for custom consulting, for example to sponsor a feature, they can contact via this form: [Development](https://about.gitlab.com/development/).

## Facebook

- We post blog articles there if we think they are good and relevant.
- Share "Inside GitLab" stories which highlight the people behind the product.

## YouTube

- We post our webcast recordings there.

## LinkedIn

- We post information for potential GitLab employees to show what it's like to work here.
- We share jobs
- We post links to relevant industry articles about open source, DevOps, or other trends and news.

## Google Plus

Not actively posting right now, maybe we should remove it from our homepage.

## Other channels

See the ones listed on our [getting help page](https://about.gitlab.com/getting-help/).
