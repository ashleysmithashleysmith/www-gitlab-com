---
layout: markdown_page
title: "Field Marketing"
---

## **Field Marketing**

Field marketing includes event marketing and swag production.  

## Events Overview

* Get GitLab team members out to meet people in the community. People that you meet in person are more likely to become evangelists for you.
* If you go talk at small events you'll see that over time you get into bigger ones.

## Events Goals
- Evangelism (brand) - Talk to as many people as we can about GitLab. Our booth presence we must be friendly, knowledgeable, and outgoing.
- Sales leads (revenue) - We will collect business cards and email addresses for potential leads.
- Hiring (company) - Always be recruiting. See someone doing a great job of evangelism for another product? Ask that person for coffee.
- Partnership (channel) - Organizations adding support for GitLab and/or shipping GitLab with their offering.
- Product Direction - what is the market asking for, in need of and willing to pay for.  
- Drive audience to any talks/ events we are having.

## How We Evaluate Events
- Price/ Budget
- Location- tops cities for developers and can we tag on another event?
- Attendees- who’s attending? Is it the right audience for us? entrepreneurs vs engineers
- Size- try to reach a large audience
- What will our presence be? Who can/ can we go, participate, speak, booth?
- We want to strike a good balance between: community and enterprise events.

## Promoting Events
- Email signatures- starting 1 month before event???
- Get attendee list and contact customers and prospects.
- BDRS will help set up 20 in person meetings set up before event to occur at event.

## **At Events**

## Employee Booth Guidelines

- If you see someone standing alone, talk to them.
- Do not stand around and talk to other GitLab coworkers. Talk to people you don’t know.
- Do not sell; generate interest to learn more. Attendees have a lot of info they are digesting, so get their info and some key info to follow up on.
- Give out swag and one-pagers!
- Don’t be afraid to walk around to other booths and talk to people. Make friends we could partner with, create interesting content with, or just have friendly beers.
- The booth should be clean and organized at all times.
- Document any product feedback you get.
- If press comes to the event feel free to put them in contact with CEO (Sid) or Marketing (Ashley).

## Networking Tips
- Listen
- Ask questions
- Don't interrupt
- Everyone gets shy sometimes, be the brave one and introduce yourself to new people.
- Feel free to talk about our next release, GitLab [strategy] (https://about.gitlab.com/strategy/), or our [product direction] (https://about.gitlab.com/direction/#vision).

## Suggested Attire
- Wear at least one branded GitLab piece of clothing.
- If the conference is business casual try some nice jeans (no holes) or pants.
- Clean shoes please.
- A smile.

## After and Event

- Fill out after event survey- send any relevant feedback on event to marketing event staff.
    - Was the event valuable?
        - Would you go again/ should we go again?
        - Did we get good leads/ contacts?
        - Best questions asked and conversations
        - Was our sponsorship/ involvement successful?
    - How was the booth set up?
        - How was the booth staffing?
        - Did the booth get enough traffic?
        - Booth location and size
    - How did our swag go over?
        - Did we have enough/ too much?
    - Contests?
- Make sure contacts/ leads gathered from event are with marketing and categorized under specific event campaign.
- Follow up with leads from event within 5 business days after event ends.


## Speaker Portal

* Catalogue of talks and speaker briefs we have done or can do and their run times.  Contact community@gitlab.com for access.

## Meetups

* We have a meetup-in-a-box package where we arrange for:

- Food (not only pizza, something healthy too)
- GitLab swag
- Walkthroughs (demo scripts)
- Exclusive content (can make new feature presentations exclusive to meetups for a month)
- Puzzles (pub quiz style, score points and compete with other cities)
- Video Q&A with service engineers and.or developers
- AMA's with CEO/CTO
-  Visit in person if we can
- Organizers become part of the GitLab Club (more details to come)

The first meetups should be run by employees. If someone manages to have 3 to 4 successive the meetup itself will live on. It is much harder to start new meetups versus maintaining existing ones. So we should everything to keep existing events going.

## Swag

* We aim to do swag in a way that doesn't take a lot of time to execute => self serve => [web shop](https://gitlab.mybrightsites.com/)
* With a webshop you can just give people credit, they can pick what they want and provide shipping info.
* Of course we love [stickers](http://opensource.com/business/15/11/open-source-stickers). Also working on special edition stickers for contributors, GitLab Club members.
* We aim to make our swag delight and or be useful.
* We aim to make limited edition swag for the community to collect.
